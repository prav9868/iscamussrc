package pojos;
// Generated 27-Mar-2019, 9:39:03 AM by Hibernate Tools 5.2.11.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Subjects generated by hbm2java
 */
@Entity
@Table(name = "subjects", catalog = "IgnouBadhegaDB")
public class Subjects implements java.io.Serializable {

	private String courseCode;
	private String courseTitle;
	private int credits;
	private Set<ProgAndSemWiseSubjects> progAndSemWiseSubjectses = new HashSet<>(0);

	public Subjects() {
	}

	public Subjects(String courseCode, String courseTitle, int credits) {
		this.courseCode = courseCode;
		this.courseTitle = courseTitle;
		this.credits = credits;
	}

	public Subjects(String courseCode, String courseTitle, int credits, Set<ProgAndSemWiseSubjects> progAndSemWiseSubjectses) {
		this.courseCode = courseCode;
		this.courseTitle = courseTitle;
		this.credits = credits;
		this.progAndSemWiseSubjectses = progAndSemWiseSubjectses;
	}

	@Id

	@Column(name = "course_code", unique = true, nullable = false, length = 20)
	public String getCourseCode() {
		return this.courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	@Column(name = "course_title", nullable = false, length = 100)
	public String getCourseTitle() {
		return this.courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	@Column(name = "credits", nullable = false)
	public int getCredits() {
		return this.credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subjects")
	public Set<ProgAndSemWiseSubjects> getProgAndSemWiseSubjectses() {
		return this.progAndSemWiseSubjectses;
	}

	public void setProgAndSemWiseSubjectses(Set<ProgAndSemWiseSubjects> progAndSemWiseSubjectses) {
		this.progAndSemWiseSubjectses = progAndSemWiseSubjectses;
	}

}
