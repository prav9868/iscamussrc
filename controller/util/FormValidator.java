/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.util;

import pojos.RegisteredUsers;
import pojos.StudyCentres;

/**
 *
 * @author praveen
 */
public class FormValidator {
    private String msg;
    public boolean isValidRegistrationFor(RegisteredUsers user, StudyCentres centre, String terms) {
        String firstname = user.getFirstname();
        String lastname = user.getLastname();
        String email = user.getEmail();
        String username = user.getUsername();
        String password = user.getPassword();
        String centreCode = user.getStudyCentreCode();
        String centreName = centre.getCentreName();
        String regionalCode = centre.getRegionalCentreCode();
        String namePatt = "^[A-za-z]+$";
        String emailPatt = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$";
        String userPatt = "^[A-Za-z][\\w]+$";
        String passPatt = "^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$";
        
        if (firstname == null || lastname == null || email == null
            || password == null || username == null || centreCode == null
            || centreName == null || regionalCode == null || firstname.length() == 0
            || lastname.length() == 0 || email.length() == 0
            || username.length() == 0 || password.length() == 0
            || centreCode.length() == 0 || centreName.length() == 0
            || regionalCode.length() == 0) {
            msg = "None of the field marked with asterisk(*) can be left empty.";
            return false;
        }
        else if (!firstname.matches(namePatt) || !lastname.matches(namePatt)) {
            msg = "Firstname and Lastname must contain only alphabets.";
            return false;
        }
        else if (!email.matches(emailPatt)) {
            msg = "Please enter a valid email address.";
            return false;
        }
        else if (!username.matches(userPatt)) {
            msg = "Username must start with alphabet followed by alpha-numeric or underscore(_) character.";
            return false;
        }
        else if (!password.matches(passPatt)) {
            msg = "Password must have 8 to 16 characters and should contain atleast one number and one special character.";
            return false;
        }
        else if (terms == null || !terms.equals("accepted")) {
            msg = "Please accept our terms and conditions.";
            return false;
        }
        return true;
    }
    
    public String errorMsg() {
        if (msg == null || msg.length() == 0) {
            return null;
        }
        return msg;
    }
}
